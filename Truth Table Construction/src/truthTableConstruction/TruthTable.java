package truthTableConstruction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import iTruthTableConstruction.ITruthTable;

/**
 * implementation of a truth table.
 */
public class TruthTable implements ITruthTable {

	/**
	 * logical expression.
	 */
	public String expression;
	
	/**
	 * the number of inputs of the logical expression.
	 */
	private int numOfPropositions;
	
	/**
	 * bit array that holds the truth values for all combinations of inputs.
	 */
	private int[] truthValues;
	
	/**
	 * number of bits in an integer (number of truth values integer can hold).
	 */
	private final int intBits = Integer.SIZE;
	
	/**
	 * number of rows of the truth table.
	 */
	private int numOfRows;
	
	/**
	 * constructor of the truth table.
	 * @param expression a logical expression.
	 * @param numOfPropositions number of inputs of the expression.
	 */
	public TruthTable(final String expression, final int numOfPropositions) {
		// removing spaces.
		this.expression = expression.replace(" ", "");
		// replace the negation of a constant.
		this.expression = this.expression.replace("0'", "1");
		this.expression = this.expression.replace("1'", "0");
		this.numOfPropositions = numOfPropositions;
		// because 1 integer can hold 32 value (2^5).
		int n = (numOfPropositions > 5)? numOfPropositions - 5 : 0;
		truthValues = new int[(int) Math.pow(2, n)];
		numOfRows = (int) Math.pow(2, numOfPropositions);
		setTable();
	}
	
	/**
	 * @return number of inputs in the expression.
	 */
	final int getNumOfPropositions() {
		return numOfPropositions;
	}
	
	/**
	 * @return the array of the truth values.
	 */
	final int[] getTable(){
		return truthValues;
	}
	
	/**
	 * checks if the entry is valid.
	 * @param index index of a row in the truth table.
	 * @return true if the index is valid.
	 */
	final private boolean entryValid(final int index) {
		if (index < 0 || index >= numOfRows) {
			return false;
		}
		return true;
	}
	
	/**
	 * checks if the entry is valid.
	 * @param entry binary entry.
	 * @return true if the entry is valid.
	 */
	final private boolean entryValid(final String entry) {
		if (entry.length() != numOfPropositions) {
			return false;
		}
		if (!entry.matches("[01]+")) {
			return false;
		}
		return true;
	}

	/**
	 * sets the corresponding bit of an index to 1 (true).
	 * @param index index of a row in the truth table.
	 */
	final private void setTruth(int index) {
		truthValues[index / intBits] |= 1 << (index % intBits);
	}
	
	/**
	 * evaluates the truth of an entry.
	 * @param entry binary representation of the propositions values.
	 * @return true if the expression is true for this entry.
	 */
	final private boolean evaluateTruth(final String entry) {
		final char firstChar = 'A';
		String expression = this.expression;
		for (int i = 0; i < numOfPropositions; i++) {
			final char token = (char) (firstChar + i);
			final char value = entry.charAt(i);
			final String negatedToken = token + "'";
			final String negatedValue = (value == '0')? "1" : "0";
			// replace negated propositions with negated values.
			expression = expression.replace(negatedToken, negatedValue);
			// replace propositions with values.
			expression = expression.replace(token, value);
		}
		// split expression into terms.
		final String[] terms = expression.split("\\+");
		final int termsLength = terms.length;
		for (int i = 0; i < termsLength; i++) {
			// if any term's propositions are all ones, then the expression is true.
			if (terms[i].matches("[1]+")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * converts integer to its binary representation with leading zeroes.
	 * @param index
	 *            an integer.
	 * @return a string of the binary representation.
	 */
	final private String indexToBinary(final int index) {
		// format of the binary (adding leading zeroes).
		final String format = '%' + String.valueOf(numOfPropositions) + 's';
		return String.format(format, Integer.toBinaryString(index)).replace(' ', '0');
	}
	
	
	/**
	 * determines the truth of the expression for a specific row.
	 * @param index index of a row in the table.
	 * @return true if corresponding bit to this row is 1.
	 */
	final private boolean getTruth(final int index) {
		if (!entryValid(index)) {
			throw new RuntimeException("Error! Invalid entry.");
		}
		if ((truthValues[index / intBits] & (1 << (index % intBits))) == 0) {
			return false;
		}
		return true;
	}
	
	@Override
	final public boolean getTruth(final String entry) {
		if (!entryValid(entry)) {
			throw new RuntimeException("Error! Invalid entry.");
		}
		final int index = Integer.parseInt(entry, 2);
		return getTruth(index);
	}

	@Override
	final public void setTable() {
		for (int i = 0; i < numOfRows; i++) {
			final String entry = indexToBinary(i);
			if (evaluateTruth(entry)) {
				setTruth(i);
			}
		}
	}

	@Override
	final public boolean isTautology() {
		for (int i = 0; i < numOfRows; i++) {
			// if any truth value is false, then the expression is not tautology.
			if (!getTruth(i)) {
				return false;
			}
		}
		return true;
	}

	@Override
	final public boolean isContradiction() {
		for (int i = 0; i < numOfRows; i++) {
			// if any truth value is true, then the expression is not contradiction.
			if (getTruth(i)) {
				return false;
			}
		}
		return true;
	}

	@Override
	final public String drawTable() {
		StringBuilder tableText = new StringBuilder();
		final char firstChar = 'A';
		tableText.append("i");
		for (int i = 0; i < numOfPropositions; i++) {
			final char temp = (char)(firstChar + i);
			tableText.append("\t" + temp);
		}
		tableText.append("\tFunction");
		for (int i = 0; i < numOfRows; i++) {
			final String binary = indexToBinary(i);
			tableText.append("\r\n" + i);
			for (int j = 0; j < numOfPropositions; j++) {
				tableText.append("\t" + binary.charAt(j));
			}
			tableText.append("\t" + (getTruth(i)? 1 : 0));
		}
		
		return tableText.toString();
	}

	@Override
	final public void printToFile(final String tableView, final String path) {
		File file = new File(path);
		try {
			if (file.exists()) {
				file.delete();	
			}
			file.createNewFile();
			FileWriter fileWriter = new FileWriter(file, false);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(tableView);
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException ioe) {
			System.out.println((ioe.getMessage()));
		}	
	}
}

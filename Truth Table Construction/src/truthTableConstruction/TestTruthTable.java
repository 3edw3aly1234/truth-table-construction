package truthTableConstruction;

import org.junit.Assert;
import org.junit.Test;

public class TestTruthTable {

	TruthTable table1;
	TruthTable table2;
	TruthTableControl control = new TruthTableControl();
	
	@Test
	public void testEquivalence() {
		table1 = new TruthTable("AB' + B'", 2);
		table2 = new TruthTable("B'", 2);
		control.setTruthTable1(table1);
		control.setTruthTable2(table2);
		Assert.assertEquals(true, control.isEquivalent());
	}
	
	@Test
	public void testEquilvalence2() {
		table1 = new TruthTable("AB'+A'B", 2);
		table2 = new TruthTable("AB+A'B'", 2);
		control.setTruthTable1(table1);
		control.setTruthTable2(table2);
		Assert.assertEquals(false, control.isEquivalent());
	}
	
	@Test
	public void testEquilvalence3() {
		table1 = new TruthTable("AB'CD+BCD'+D'A'", 5);
		table2 = new TruthTable("AB'CDE'+AB'CDE+BCD'+D'A'+A'D'C'", 5);
		control.setTruthTable1(table1);
		control.setTruthTable2(table2);
		Assert.assertEquals(true, control.isEquivalent());
	}
	
	@Test
	public void testTautology() {
		table1 = new TruthTable("A + A'", 1);
		Assert.assertEquals(true, table1.isTautology());
	}
	
	@Test
	public void testContradiction() {
		table1 = new TruthTable("AA'", 1);
		Assert.assertEquals(true, table1.isContradiction());
	}
	
	@Test
	public void testValidity() {
		Assert.assertEquals(false, control.isValid("0''1'", 2));
	}
	
	@Test
	public void testValidity1() {
		Assert.assertEquals(true, control.isValid("0'1'", 2));
	}
	
	@Test
	public void testValidity2() {
		Assert.assertEquals(false, control.isValid("'AB'", 2));
	}
	
	@Test
	public void testValidity3() {
		Assert.assertEquals(false, control.isValid("AB''", 2));
	}
	
	@Test
	public void testValidity4() {
		Assert.assertEquals(false, control.isValid("AB' ++ A", 2));
	}
	
	@Test
	public void testValidity5() {
		Assert.assertEquals(false, control.isValid("AB' + A+", 2));
	}
	
	@Test
	public void testValidity6() {
		Assert.assertEquals(false, control.isValid("+AB' + A", 2));
	}
	
	@Test
	public void testDrawTable() {
		table1 = new TruthTable("AB'+BA'", 2);
		final String text = table1.drawTable();
		final String expected = "i\tA\tB\tFunction\r\n" + 
				"0\t0\t0\t0\r\n" + 
				"1\t0\t1\t1\r\n" + 
				"2\t1\t0\t1\r\n" + 
				"3\t1\t1\t0";
		Assert.assertEquals(expected, text);
	}
	
	@Test
	public void testGetTruth() {
		table1 = new TruthTable("AB'+BA'", 2);
		Assert.assertEquals(true, table1.getTruth("10"));
	}
}

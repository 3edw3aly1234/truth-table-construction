package truthTableConstruction;

/**
 * a class to control truth tables
 * having the ability to compare between them.
 */
public class TruthTableControl {
	
	/**
	 * controller contains two truth tables to be able to compare between them.
	 */
	private TruthTable truthTable1, truthTable2;
	
	/**
	 * empty constructor.
	 */
	public TruthTableControl() {
	}
	
	/**
	 * constructor of the controller sets the two truth tables.
	 * @param truthTable1 first truth table.
	 * @param truthTable2 second truth table.
	 */
	public TruthTableControl(final TruthTable truthTable1, final TruthTable truthTable2) {
		this.truthTable1 = truthTable1;
		this.truthTable2 = truthTable2;
	}
	
	/**
	 * sets the first truth table.
	 * @param truthTable a truth table instance.
	 */
	public void setTruthTable1(final TruthTable truthTable) {
		truthTable1 = truthTable;
	}
	
	/**
	 * sets the second truth table.
	 * @param truthTable a truth table instance.
	 */
	public void setTruthTable2(final TruthTable truthTable) {
		truthTable2 = truthTable;
	}
	
	/**
	 * determines the correctness of a logical expression.
	 * @param expression
	 *            a logical expression (SOP).
	 * @param numOfPropositions
	 *            the number of propositions in expression.
	 * @return true if the expression is valid.
	 */
	final public boolean isValid(final String expression, final int numOfPropositions) {
		// check if number of propositions is invalid.
		// our program would solve for up to 15 propositions.
		if (numOfPropositions < 1 || numOfPropositions > 15) {
			return false;
		}
		// removing spaces.
		final String exp = expression.replace(" ", "");
		if (exp.equals("")) {
			return false;
		}
		// check if the expression has a trailing operation.
		if (exp.charAt(exp.length() - 1) == '+') {
			return false;
		}
		// split the expression into terms.
		String[] terms = exp.split("\\+");
		final int termsLength = terms.length;
		for (int i = 0; i < termsLength; i++) {
			final String token = terms[i];
			// check if there is excessive operation inside the expression.
			if (token.equals("")) {
				return false;
			}
			final int length = token.length();
			// check if the term has excessive negation.
			// ' (single quotation) represents logical NOT.
			if (token.charAt(0) == '\'') {
				return false;
			}
			if (token.contains("''")) {
				return false;
			}
			for (int j = 0; j < length; j++) {
				final char proposition = token.charAt(j);
				// check if there is a strange character in the expression.
				// for example: if number of propositions is 4
				// characters should belong to {'A', 'B', 'C', 'D', '\'', '0', '1'}.
				if (proposition != '0' && proposition != '1' && proposition != '\'' &&
						(proposition < 'A' || proposition >= 'A' + numOfPropositions)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * determines if logical expression is equivalent to another one by comparing
	 * their truth tables.
	 * @return true if truth values are equivalent for all combinations of
	 *         propositions.
	 */
	final public boolean isEquivalent() {
		final int numOfPropositions1 = truthTable1.getNumOfPropositions();
		final int numOfPropositions2 = truthTable2.getNumOfPropositions();
		// two expressions must have the same number of propositions to be equal.
		if (numOfPropositions1 != numOfPropositions2) {
			return false;
		}
		int[] table1 = truthTable1.getTable();
		int[] table2 = truthTable2.getTable();
		final int lengthOfTable = table1.length;
		for (int i = 0; i < lengthOfTable; i++) {
			// if any corresponding truth values are not equal, then the expressions are not equal.
			if (table1[i] != table2[i]) {
				return false;
			}
		}
		return true;
	}
}
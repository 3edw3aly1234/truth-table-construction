package truthTableConstructionGui;
import truthTableConstruction.TruthTable;
import truthTableConstruction.TruthTableControl;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Cursor;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
public class TruthTableGUI {

	private JFrame frame;
	private JPanel cards;
	private JPanel mainMenucard;
	private JPanel expressionCard;
	private JTextField firstExpText;
	private JTextField secondExpText;
	private JTextField binaryEntryTextField;
	private JButton lightBtn;
	private JButton helpBtn;
	private JLabel firstExpLabel;
	private JButton firstExpBtn;
	private JLabel secondExpLabel;
	private JButton secondExpBtn;
	JLabel equalAnswerLabel;
	
	JButton lightBtn2;
	JButton helpBtn2;
	JScrollPane functionScrollPane;
	JTextArea functionTextArea;
	JScrollPane truthTableScrollPane;
	JTextArea truthTableTextArea;
	JLabel tautologyLabel;
	JLabel contradictionLabel;
	JLabel lblCheckForInputs;
	JButton checkBtn;
	JTextArea checkResult;
	JButton drawBtn;
	JButton saveBtn;
	JLabel tautologyAns;
	JLabel contradictionAns;
	JButton backBtn;
	private JTextField firstExpNum;
	private JTextField secondExpNum;
	
	private TruthTable table1;
	private TruthTable table2;
	
	private boolean table1Drawn = false;
	private boolean table2Drawn = false;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TruthTableGUI window = new TruthTableGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TruthTableGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Truth Table");
		frame.setResizable(false);
		frame.setBounds(100, 100, 436, 239);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("./icon/table.png"));
		
		mainMenucard = new JPanel();
		mainMenucard.setBackground(Color.WHITE);
		expressionCard = new JPanel();
		expressionCard.setBackground(Color.WHITE);
		
		cards = new JPanel(new CardLayout());
		cards.add(mainMenucard, "mainMenucard");
		mainMenucard.setLayout(null);
		
		firstExpText = new JTextField();
		firstExpText.setToolTipText("Enter an expression");
		firstExpText.setDisabledTextColor(Color.BLACK);
		firstExpText.setFont(new Font("Calibri", Font.BOLD, 13));
		firstExpText.setBorder(new LineBorder(Color.BLACK));
		firstExpText.setBounds(202, 80, 69, 20);
		mainMenucard.add(firstExpText);
		firstExpText.setColumns(10);
		
		firstExpLabel = new JLabel("Expression 1");
		firstExpLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		firstExpLabel.setBounds(105, 83, 70, 14);
		mainMenucard.add(firstExpLabel);
		
		secondExpLabel = new JLabel("Expression 2");
		secondExpLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		secondExpLabel.setBounds(105, 108, 70, 14);
		mainMenucard.add(secondExpLabel);
		
		secondExpText = new JTextField();
		secondExpText.setToolTipText("Enter an expression");
		secondExpText.setDisabledTextColor(Color.BLACK);
		secondExpText.setFont(new Font("Calibri", Font.BOLD, 13));
		secondExpText.setColumns(10);
		secondExpText.setBorder(new LineBorder(Color.BLACK));
		secondExpText.setBounds(202, 105, 69, 20);
		mainMenucard.add(secondExpText);
		
		Icon enterIcon = new ImageIcon("./icon/enter.png");
		
		firstExpBtn = new JButton("");
		firstExpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchWindow("Expression 1");
			}
		});
		firstExpBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		firstExpBtn.setToolTipText("View expression 1 page");
		firstExpBtn.setBackground(Color.WHITE);
		firstExpBtn.setBorder(null);
		firstExpBtn.setBounds(273, 80, 20, 20);
		firstExpBtn.setIcon(enterIcon);
		mainMenucard.add(firstExpBtn);
		
		secondExpBtn = new JButton("");
		secondExpBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switchWindow("Expression 2");
			}
		});
		secondExpBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		secondExpBtn.setToolTipText("View expression 2 page");
		secondExpBtn.setBackground(Color.WHITE);
		secondExpBtn.setBorder(null);
		secondExpBtn.setBounds(273, 105, 20, 20);
		secondExpBtn.setIcon(enterIcon);
		mainMenucard.add(secondExpBtn);
		
		JLabel equivalentLabel = new JLabel("<HTML><U>Are they equivalent?</U></HTML>");
		equivalentLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				final int numOfPropositions1;
				final String expression1;
				final int numOfPropositions2;
				final String expression2;
				try {
					numOfPropositions1 = Integer.parseInt(firstExpNum.getText());
				} catch (Exception e) {
					equalAnswerLabel.setText("Error in expressions!");
					return;
				}
				expression1 = firstExpText.getText();
				try {
					numOfPropositions2 = Integer.parseInt(secondExpNum.getText());
				} catch (Exception e) {
					equalAnswerLabel.setText("Error in expressions!");
					return;
				}
				expression2 = secondExpText.getText();
				
				if(expression1.equals("") || expression2.equals("")) {
					equalAnswerLabel.setText("Error in expressions!");
					return;
				}
				
				TruthTableControl tableControl = new TruthTableControl();
				if (!tableControl.isValid(expression1, numOfPropositions1)) {
					equalAnswerLabel.setText("Error in expressions!");
					return;
				}
				if (!tableControl.isValid(expression2, numOfPropositions2)) {
					equalAnswerLabel.setText("Error in expressions!");
					return;
				}
				TruthTable table1 = new TruthTable(expression1, numOfPropositions1);
				TruthTable table2 = new TruthTable(expression2, numOfPropositions2);
				tableControl.setTruthTable1(table1);
				tableControl.setTruthTable2(table2);
				if (tableControl.isEquivalent()) {
					equalAnswerLabel.setText("Yes!");
				} else {
					equalAnswerLabel.setText("No!");
				}
			}
		});
		equivalentLabel.setToolTipText("Compare between the truth tables of the two expressions");
		equivalentLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		equivalentLabel.setForeground(Color.BLUE);
		equivalentLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		equivalentLabel.setBounds(105, 133, 128, 20);
		mainMenucard.add(equivalentLabel);
		
		equalAnswerLabel = new JLabel("");
		equalAnswerLabel.setHorizontalAlignment(SwingConstants.LEFT);
		equalAnswerLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		equalAnswerLabel.setBounds(229, 139, 116, 14);
		mainMenucard.add(equalAnswerLabel);
		
		Icon light = new ImageIcon("./icon/light.png");
		lightBtn = new JButton("");
		lightBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Color black = Color.BLACK;
				final Color white = Color.WHITE;
				if (mainMenucard.getBackground().equals(white)) {
					mainMenucard.setBackground(black);
					lightBtn.setBackground(black);
					helpBtn.setBackground(black);
					firstExpLabel.setForeground(white);
					secondExpLabel.setForeground(white);
					firstExpText.setForeground(white);
					firstExpText.setBackground(black);
					firstExpText.setBorder(new LineBorder(white));
					secondExpText.setForeground(white);
					secondExpText.setBackground(black);
					secondExpText.setBorder(new LineBorder(white));
					firstExpBtn.setBackground(black);
					secondExpBtn.setBackground(black);
					equalAnswerLabel.setForeground(white);
					firstExpNum.setForeground(white);
					firstExpNum.setBackground(black);
					firstExpNum.setBorder(new LineBorder(white));
					secondExpNum.setForeground(white);
					secondExpNum.setBackground(black);
					secondExpNum.setBorder(new LineBorder(white));
					if (expressionCard.getBackground().equals(white)) {
						lightBtn2.doClick();
					}
				} else {
					mainMenucard.setBackground(white);
					lightBtn.setBackground(white);
					helpBtn.setBackground(white);
					firstExpLabel.setForeground(black);
					secondExpLabel.setForeground(black);
					firstExpText.setForeground(black);
					firstExpText.setBackground(white);
					firstExpText.setBorder(new LineBorder(black));
					secondExpText.setForeground(black);
					secondExpText.setBackground(white);
					secondExpText.setBorder(new LineBorder(black));
					firstExpBtn.setBackground(white);
					secondExpBtn.setBackground(white);
					equalAnswerLabel.setForeground(black);
					firstExpNum.setForeground(black);
					firstExpNum.setBackground(white);
					firstExpNum.setBorder(new LineBorder(black));
					secondExpNum.setForeground(black);
					secondExpNum.setBackground(white);
					secondExpNum.setBorder(new LineBorder(black));
					if (expressionCard.getBackground().equals(black)) {
						lightBtn2.doClick();
					}
				}
			}
		});
		lightBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lightBtn.setFocusable(false);
		lightBtn.setToolTipText("Turn Lights On/Off");
		lightBtn.setBorder(null);
		lightBtn.setBackground(Color.WHITE);
		lightBtn.setBounds(10, 11, 20, 20);
		lightBtn.setIcon(light);
		mainMenucard.add(lightBtn);
		
		Icon help = new ImageIcon("./icon/help.png");
		helpBtn = new JButton("");
		helpBtn.addMouseListener( new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				final String userGuide = "-	The given expression is in SOP form. (Any other form is treated as invalid expression).\r\n" + 
						"\r\n" + 
						"- \" AB'C + A'C + BD'A' \" is Correct\r\n" + 
						"- \" A(B+C) \" is Incorrect\r\n" + 
						"- \" 'A + C'' \" is Incorrect\r\n" + 
						"- \" XYZ + + G \" is Incorrect\r\n" + 
						"- \" 0'CX + 1PX' \"  ==  \" CX + PX \" is Correct\r\n" + 
						"\r\n" + 
						"- AND, OR and NOT are the only accepted logical operations in expression.\r\n" + 
						"- AND is denoted by nothing.\r\n" + 
						"- OR is denoted by  +  operation.\r\n" + 
						"- NOT is denoted by  '  character (single quote).\r\n" + 
						"\r\n" + 
						"-	User must enter the propositions according to the alphapets:\r\n" + 
						"    if the number of propositions  = 4\r\n" + 
						"    user can only enter A, B, C and D\r\n" + 
						"    if the number of propositions  = 6\r\n" + 
						"    user can only enter A, B, C, D, E and F\r\n" + 
						"    (beside 0's and 1's and the available operations of course).\r\n" + 
						"\r\n-	The maximum number of propositions of the expression is 15.";
				JOptionPane.showMessageDialog(null, userGuide, "Help", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		helpBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		helpBtn.setFocusable(false);
		helpBtn.setToolTipText("Help");
		helpBtn.setBorder(null);
		helpBtn.setBackground(Color.WHITE);
		helpBtn.setBounds(32, 11, 20, 20);
		helpBtn.setIcon(help);
		mainMenucard.add(helpBtn);
		
		firstExpNum = new JTextField();
		firstExpNum.setToolTipText("Number of propositions (inputs)");
		firstExpNum.setFont(new Font("Calibri", Font.BOLD, 13));
		firstExpNum.setBorder(new LineBorder(Color.BLACK));
		firstExpNum.setBounds(178, 80, 20, 20);
		mainMenucard.add(firstExpNum);
		firstExpNum.setColumns(10);
		
		secondExpNum = new JTextField();
		secondExpNum.setToolTipText("Number of propositions (inputs)");
		secondExpNum.setFont(new Font("Calibri", Font.BOLD, 13));
		secondExpNum.setBorder(new LineBorder(Color.BLACK));
		secondExpNum.setColumns(10);
		secondExpNum.setBounds(178, 105, 20, 20);
		mainMenucard.add(secondExpNum);
		
		cards.add(expressionCard, "expressionCard");
		expressionCard.setLayout(null);
		
		lightBtn2 = new JButton("");
		lightBtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Color white = Color.WHITE;
				final Color black = Color.BLACK;
				if (expressionCard.getBackground().equals(white)) {
					expressionCard.setBackground(black);
					lightBtn2.setBackground(black);
					helpBtn2.setBackground(black);
					functionScrollPane.setBackground(black);
					functionScrollPane.setBorder(new LineBorder(white));
					functionTextArea.setBackground(black);
					functionTextArea.setForeground(white);
					truthTableScrollPane.setBackground(black);
					truthTableScrollPane.setBorder(new LineBorder(white));
					truthTableTextArea.setBackground(black);
					truthTableTextArea.setForeground(white);
					lblCheckForInputs.setForeground(white);
					binaryEntryTextField.setBackground(black);
					binaryEntryTextField.setForeground(white);
					binaryEntryTextField.setBorder(new LineBorder(white));
					checkBtn.setBackground(black);
					checkResult.setBackground(black);
					checkResult.setForeground(white);
					checkResult.setBorder(new LineBorder(white));
					drawBtn.setBackground(black);
					saveBtn.setBackground(black);
					tautologyAns.setForeground(white);
					contradictionAns.setForeground(white);
					backBtn.setBackground(black);
					if (mainMenucard.getBackground().equals(white)) {
						lightBtn.doClick();
					}
				} else {
					expressionCard.setBackground(white);
					lightBtn2.setBackground(white);
					helpBtn2.setBackground(white);
					functionScrollPane.setBackground(white);
					functionTextArea.setBackground(white);
					functionTextArea.setForeground(black);
					truthTableScrollPane.setBackground(white);
					truthTableTextArea.setBackground(white);
					truthTableTextArea.setForeground(black);
					lblCheckForInputs.setForeground(black);
					binaryEntryTextField.setBackground(white);
					binaryEntryTextField.setForeground(black);
					binaryEntryTextField.setBorder(new LineBorder(black));
					checkBtn.setBackground(white);
					checkResult.setBackground(white);
					checkResult.setForeground(black);
					checkResult.setBorder(new LineBorder(black));
					drawBtn.setBackground(white);
					saveBtn.setBackground(white);
					tautologyAns.setForeground(black);
					contradictionAns.setForeground(black);
					backBtn.setBackground(white);
					if (mainMenucard.getBackground().equals(black)) {
						lightBtn.doClick();
					}
				}
			}
		});
		lightBtn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lightBtn2.setToolTipText("Turn Lights On/Off");
		lightBtn2.setFocusable(false);
		lightBtn2.setBorder(null);
		lightBtn2.setBackground(Color.WHITE);
		lightBtn2.setBounds(10, 11, 20, 20);
		lightBtn2.setIcon(light);
		expressionCard.add(lightBtn2);
		
		helpBtn2 = new JButton("");
		helpBtn2.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				final String userGuide = "-	The given expression is in SOP form. (Any other form is treated as invalid expression).\r\n" + 
						"\r\n" + 
						"- \" AB'C + A'C + BD'A' \" is Correct\r\n" + 
						"- \" A(B+C) \" is Incorrect\r\n" + 
						"- \" 'A + C'' \" is Incorrect\r\n" + 
						"- \" XYZ + + G \" is Incorrect\r\n" + 
						"- \" 0'CX + 1PX' \"  ==  \" CX + PX \" is Correct\r\n" + 
						"\r\n" + 
						"- AND, OR and NOT are the only accepted logical operations in expression.\r\n" + 
						"- AND is denoted by nothing.\r\n" + 
						"- OR is denoted by  +  operation.\r\n" + 
						"- NOT is denoted by  '  character (single quote).\r\n" + 
						"\r\n" + 
						"-	User must enter the propositions according to the alphapets:\r\n" + 
						"    if the number of propositions  = 4\r\n" + 
						"    user can only enter A, B, C and D\r\n" + 
						"    if the number of propositions  = 6\r\n" + 
						"    user can only enter A, B, C, D, E and F\r\n" + 
						"    (beside 0's and 1's and the available operations of course).\r\n" + 
						"\r\n-	The maximum number of propositions of the expression is 15.";
				JOptionPane.showMessageDialog(null, userGuide, "Help", JOptionPane.INFORMATION_MESSAGE);
			}
		}

				
		);
		helpBtn2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		helpBtn2.setToolTipText("Help");
		helpBtn2.setFocusable(false);
		helpBtn2.setBorder(null);
		helpBtn2.setBackground(Color.WHITE);
		helpBtn2.setBounds(32, 11, 20, 20);
		helpBtn2.setIcon(help);
		expressionCard.add(helpBtn2);
		
		
		
		functionTextArea = new JTextArea();
		functionTextArea.setCaretColor(Color.WHITE);
		functionTextArea.setBorder(null);
		functionTextArea.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		functionTextArea.setEditable(false);
		functionTextArea.setFont(new Font("Calibri", Font.BOLD, 13));
		functionTextArea.setToolTipText("Function Logical Expression");
		functionTextArea.setBounds(10, 42, 150, 29);
		
		functionScrollPane = new JScrollPane(functionTextArea);
		functionScrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		functionScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		functionScrollPane.getHorizontalScrollBar().setUnitIncrement(50);
		functionScrollPane.getVerticalScrollBar().setUnitIncrement(50);
		functionScrollPane.setBackground(Color.WHITE);
		functionScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		functionScrollPane.setBounds(8, 40, 201, 39);
		
		expressionCard.add(functionScrollPane);
		
		truthTableScrollPane = new JScrollPane((Component) null);
		truthTableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		truthTableScrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		truthTableScrollPane.setBackground(Color.WHITE);
		truthTableScrollPane.setBounds(8, 88, 400, 185);
		expressionCard.add(truthTableScrollPane);
		
		truthTableTextArea = new JTextArea();
		truthTableTextArea.setEditable(false);
		truthTableTextArea.setBorder(null);
		truthTableTextArea.setFont(new Font("Calibri", Font.BOLD, 13));
		truthTableScrollPane.setViewportView(truthTableTextArea);
		
		tautologyLabel = new JLabel("<HTML><U>Is Tautology?</U></HTML>");
		tautologyLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				TruthTable table;
				if (frame.getTitle().equals("Expression 1")) {
					table = table1;
				} else {
					table = table2;
				}
				if (table.isTautology()) {
					tautologyAns.setText("Yes!");
				} else {
					tautologyAns.setText("No!");
				}
			}
		});
		tautologyLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tautologyLabel.setToolTipText("Check if the expression is tautology");
		tautologyLabel.setForeground(Color.BLUE);
		tautologyLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		tautologyLabel.setBounds(266, 40, 70, 17);
		expressionCard.add(tautologyLabel);
		
		contradictionLabel = new JLabel("<HTML><U>Is Contradiction?</U></HTML>");
		contradictionLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				TruthTable table;
				if (frame.getTitle().equals("Expression 1")) {
					table = table1;
				} else {
					table = table2;
				}
				if (table.isContradiction()) {
					contradictionAns.setText("Yes!");
				} else {
					contradictionAns.setText("No!");
				}
			}
		});
		contradictionLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		contradictionLabel.setToolTipText("Check if the expression is contradiction");
		contradictionLabel.setForeground(Color.BLUE);
		contradictionLabel.setFont(new Font("Calibri", Font.BOLD, 13));
		contradictionLabel.setBounds(266, 59, 91, 17);
		expressionCard.add(contradictionLabel);
		
		lblCheckForInputs = new JLabel("Check for inputs");
		lblCheckForInputs.setHorizontalAlignment(SwingConstants.CENTER);
		lblCheckForInputs.setToolTipText("");
		lblCheckForInputs.setForeground(Color.BLACK);
		lblCheckForInputs.setFont(new Font("Calibri", Font.BOLD, 13));
		lblCheckForInputs.setBounds(73, 12, 88, 17);
		expressionCard.add(lblCheckForInputs);
		
		binaryEntryTextField = new JTextField();
		binaryEntryTextField.setToolTipText("Binary input");
		binaryEntryTextField.setFont(new Font("Calibri", Font.BOLD, 13));
		binaryEntryTextField.setBorder(new LineBorder(Color.BLACK));
		binaryEntryTextField.setHorizontalAlignment(SwingConstants.CENTER);
		binaryEntryTextField.setBounds(169, 12, 86, 17);
		expressionCard.add(binaryEntryTextField);
		binaryEntryTextField.setColumns(10);
		
		checkBtn = new JButton("");
		checkBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final String binaryEntry = binaryEntryTextField.getText();
				TruthTable table;
				if (frame.getTitle().equals("Expression 1")) {
					table = table1;
				} else {
					table = table2;
				}
				try {
					boolean result = table.getTruth(binaryEntry);
					final String value = result? "1" : "0";
					checkResult.setText(value);
				} catch (Exception e1) {
					return;
				}
			}
		});
		checkBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		checkBtn.setToolTipText("Check");
		checkBtn.setFocusable(false);
		checkBtn.setBorder(null);
		checkBtn.setBackground(Color.WHITE);
		checkBtn.setBounds(256, 11, 20, 20);
		Icon check = new ImageIcon("./icon/check.png");
		checkBtn.setIcon(check);
		expressionCard.add(checkBtn);
		
		checkResult = new JTextArea();
		checkResult.setFont(new Font("Calibri", Font.BOLD, 13));
		checkResult.setBorder(new LineBorder(new Color(0, 0, 0)));
		checkResult.setToolTipText("Result");
		checkResult.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		checkResult.setBounds(288, 12, 20, 17);
		expressionCard.add(checkResult);
		
		drawBtn = new JButton("");
		drawBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TruthTable table;
				if (frame.getTitle().equals("Expression 1")) {
					if (table1Drawn) {
						return;
					}
					table = table1;
					table1Drawn = true;
				} else {
					if (table2Drawn) {
						return;
					}
					table = table2;
					table2Drawn = true;
				}
				truthTableTextArea.setText(table.drawTable());
			}
		});
		drawBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		drawBtn.setToolTipText("Draw Truth Table");
		drawBtn.setFocusable(false);
		drawBtn.setBorder(null);
		drawBtn.setBackground(Color.WHITE);
		drawBtn.setBounds(219, 40, 20, 20);
		Icon draw = new ImageIcon("./icon/draw.png");
		drawBtn.setIcon(draw);
		expressionCard.add(drawBtn);
		
		saveBtn = new JButton("");
		saveBtn.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				TruthTable table;
				final boolean draw;
				if (frame.getTitle().equals("Expression 1")) {
					table = table1;
					draw = table1Drawn;
				} else {
					table = table2;
					draw = table2Drawn;
				}
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter filter1 = new FileNameExtensionFilter("Text", "txt");
				fileChooser.addChoosableFileFilter(filter1);
				fileChooser.setAcceptAllFileFilterUsed(false);
				final int choice = fileChooser.showSaveDialog(frame);
				if (choice == fileChooser.APPROVE_OPTION) {
					final String path = fileChooser.getSelectedFile().getAbsolutePath();
					if (draw) {
						table.printToFile(truthTableTextArea.getText(), path + ".txt");
					} else {
						table.printToFile(table.drawTable(), path + ".txt");
					}
				}
			}
		});
		saveBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		saveBtn.setToolTipText("Save Truth Table to a File");
		saveBtn.setFocusable(false);
		saveBtn.setBorder(null);
		saveBtn.setBackground(Color.WHITE);
		saveBtn.setBounds(219, 59, 20, 20);
		Icon save = new ImageIcon("./icon/save.png");
		saveBtn.setIcon(save);
		expressionCard.add(saveBtn);
		
		tautologyAns = new JLabel("");
		tautologyAns.setFont(new Font("Calibri", Font.BOLD, 13));
		tautologyAns.setBounds(362, 43, 25, 14);
		expressionCard.add(tautologyAns);
		
		contradictionAns = new JLabel("");
		contradictionAns.setFont(new Font("Calibri", Font.BOLD, 13));
		contradictionAns.setBounds(362, 63, 25, 14);
		expressionCard.add(contradictionAns);
		
		backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setSize(436, 239);
				CardLayout c = (CardLayout)(cards.getLayout());
				frame.setTitle("Truth Table");
				c.show(cards, "mainMenucard");
			}
		});
		backBtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		backBtn.setToolTipText("Back to Main Menu");
		backBtn.setFocusable(false);
		backBtn.setBorder(null);
		backBtn.setBackground(Color.WHITE);
		backBtn.setBounds(388, 11, 20, 20);
		Icon back = new ImageIcon("./icon/back.png");
		backBtn.setIcon(back);
		expressionCard.add(backBtn);
		
		frame.getContentPane().add(cards, BorderLayout.CENTER);
	}
	
	final private void switchWindow(final String expressionName) {
		final int numOfPropositions;
		final String expression;
		if (expressionName.equals("Expression 1")) {
			try {
				numOfPropositions = Integer.parseInt(firstExpNum.getText());
			} catch (Exception e) {
				return;
			}
			expression = firstExpText.getText();
		} else {
			try {
				numOfPropositions = Integer.parseInt(secondExpNum.getText());
			} catch (Exception e) {
				return;
			}
			expression = secondExpText.getText();
		}
		
		if(expression.equals("")) {
			return;
		}
		
		TruthTableControl tableControl = new TruthTableControl();
		if (!tableControl.isValid(expression, numOfPropositions)) {
			return;
		}
		if (expressionName.equals("Expression 1")) {
			table1 = new TruthTable(expression, numOfPropositions);
		} else {
			table2 = new TruthTable(expression, numOfPropositions);
		}
		StringBuilder functionName = new StringBuilder();
		functionName.append("F(A");
		final char firstChar = 'B';
		for (int i = 0; i < numOfPropositions - 1; i++) {
			functionName.append(", " + (char) (firstChar + i));
		}
		functionName.append(") = ");
		if (expressionName.equals("Expression 1")) {
			functionName.append(table1.expression);
		} else {
			functionName.append(table2.expression);
		}
		functionTextArea.setText(functionName.toString());
		truthTableTextArea.setText("");
		binaryEntryTextField.setText("");
		checkResult.setText("");
		tautologyAns.setText("");
		contradictionAns.setText("");
		table1Drawn = false;
		table2Drawn = false;
		frame.setSize(422, 310);
		CardLayout c = (CardLayout)(cards.getLayout());
		frame.setTitle(expressionName);
		c.show(cards, "expressionCard");
	}
}

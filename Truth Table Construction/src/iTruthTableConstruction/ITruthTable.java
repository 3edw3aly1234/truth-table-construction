package iTruthTableConstruction;

/**
 * interface of the truth table important methods.
 */
public interface ITruthTable {

	/**
	 * determines the truth of the expression for a specific row.
	 * @param entry
	 *            binary input.
	 * @return true if corresponding bit to this entry is 1.
	 */
	public boolean getTruth(final String entry);

	/**
	 * evaluates truth for all combinations of propositions.
	 */
	public void setTable();

	/**
	 * determines if a logical expression is tautology.
	 * @return true if all the truth values are true.
	 */
	public boolean isTautology();

	/**
	 * determines if a logical expression is contradiction.
	 * @return true if all the truth values are false.
	 */
	public boolean isContradiction();

	/**
	 * constructs a visual view of a truth table.
	 * @return a string representation of the table view.
	 */
	public String drawTable();

	/**
	 * prints the visual view of truth table to a text file given its path.
	 * @param tableView
	 *            a string representation of the table view.
	 * @param path
	 *            the file path in the system.
	 */
	public void printToFile(final String tableView, final String path);
}
